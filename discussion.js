// CRUD Operations

// Insert Documents (CREATE)

/*
	Syntax:
		Insert One Document
			-db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Document
			-db.collectionName.insertMany([
				{
				"fieldA": "valueA",
				"fieldB": "valueB"
				},
				{
				"fieldA": "valueA",
				"fieldB": "valueB"
				}
			]);



*/
// upon execution on Robo 3T make sure not to execute your data several time as it will duplicate


db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@gmail.com",
	"company": "none"
});


db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "nielarmstrong@gmail.com",
			"department": "none"
		}

	])

db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}

	])

// Find Documents (Read)

/*
	Syntax:
		-db.collectionName.find() - this will retrieve all the documents.

		-db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		-db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match our criteria

		-db.collectionName.findOne({}) - this will return the first document in our collection

*/

db.users.find();

db.users.find({
		"firstName":"Jane"
	});

db.users.find({
		"firstName":"Neil",
		"age": 82
	});



// Updating documents (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
					"fieldToBeUpdated": "updatedValue"
			}
		})

	- updating first matching document on our collection

	Syntax: Multiple

	db.collectionName.updateMany(
		{
			"criteria":"value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})

*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"department": "none"
});

// Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
)

// Updating Multiple Documents
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}
)

db.users.updateOne({},
		{
			$set: {
				"department": "Operations"
			}
		}
	)

db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}

)

/*
	Mini Activity:
*/


db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
)

db.courses.updateMany({},

	{
		$set: {
			"enrollees": 10
		}
	}
)


// Deleting Documents (DELETE)
db.users.insertOne({
	"firstName": "Test",
})

// Deliting a single document
/*
	db.collectionName.deleteOne({"criteria": "value"})
*/

db.users.deleteOne({
	"firstName": "Test"
})

// Deliting a multiple document
/*
	db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteMany({
	"dept": "HR"
})
// update Jane's dept

db.courses.deleteMany({}) 
// will delete all course documents




